﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraDirector : MonoBehaviour {

    public Canvas canvas;

    public RectTransform secondCameraRoot; 
    public RectTransform thirdCameraRoot;

    CodeAnimator codeAnimator = new CodeAnimator();
   
    public void FocusThirdCamera(float duration)
    {
        FocusThirdCamera();
        if (duration > 0)
        {
            codeAnimator.StartWaitAndExecute(this, Mathf.Abs(duration), SetNormal, false);
        }
    }

    public void FocusSecondCamera(float duration = -1)
    {
        FocusThirdCamera();
        if (duration > 0)
        {
            codeAnimator.StartWaitAndExecute(this, Mathf.Abs(duration), SetNormal, false);
        }
    }


    public void FocusThirdCamera()
    {

    }

    public void FocusSecondCamera()
    {

    }

    public void ActivateSecondCamera()
    {

    }

    public void ActivateThirdCamera()
    {

    }

    public void DeactivateSecondCamera()
    {

    }

    public void DeactivateThirdCamera()
    {

    }

    public void SetNormal()
    {

    }
}
