﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaCannonUI : MonoBehaviour
{
    public AlphaCannon cannon;
    public Transform realCannonPoint;

    public Transform targeRender;

    public Vector3 targetPoint;


    public void FingerDown()
    {
        targeRender.gameObject.SetActive(true);
    }

    public void FingerUp()
    {
        targeRender.gameObject.SetActive(false);
    }

    public void SetTargetPoint(Vector2 pos)
    {
        targetPoint = pos;
    }

    bool linesIntersect;
    bool segmentsIntersect;
    Vector2 intersection;
    Vector2 cP1;
    Vector2 cP2;

    private void Update()
    {
        if (targeRender.gameObject.activeSelf)
        {
            targeRender.transform.position = targetPoint;
            LineUtils.FindIntersection(cannon.firePoint.position, cannon.transform.position, targeRender.position, targeRender.position + Vector3.up, out linesIntersect,out segmentsIntersect, out intersection, out cP1,out cP2);
            realCannonPoint.position = intersection;
        }
    }

    
}
