﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;
using Sirenix.OdinInspector;

public class AlphaCannon : MechWeaponLongSwipe
{
    [TabGroup("Dependencys"),OnValueChanged("CheckAlphaCanonUi")]
    public GameObject uiPrefab;
    [TabGroup("Dependencys")]
    public GameObject proyectilePrefab;

    [TabGroup("Dependencys")]
    public Transform canonPivot;

    [TabGroup("Dependencys")]
    public Transform firePoint;

    private Proyectile lastProyectile;

    private AlphaCannonUI ui;

    [TabGroup("Main")]
    public float aimingSpeed = 20;
    [TabGroup("Main")]
    public float smoothTime = 0;
    [TabGroup("Main"), ShowInInspector,ReadOnly]
    private bool aiming = false;
    Vector2 lookAtPosition;
    Vector2 diference;
    Vector2 dir;


    CodeAnimator codeAnimator = new CodeAnimator();
    [TabGroup("Animation")]
    public AnimationCurve curve;
    public float duration = .5f;
    public float magnitud = 1;


    public void CheckAlphaCanonUi()
    {
        if (uiPrefab.GetComponent<AlphaCannonUI>() == null) uiPrefab = null;
    }

    public override void Fire()
    {
        if (!CanFire)
            return;
        base.Fire();

        lastProyectile = Instantiate(proyectilePrefab, null).GetComponent<Proyectile>();
        lastProyectile.transform.position = firePoint.position;
        lastProyectile.Activate();
        lastProyectile.transform.rotation = canonPivot.rotation;

        codeAnimator.StartAnimacion(this, x =>
         {
             canonPivot.localPosition = -canonPivot.right * x * magnitud;
         }, DeltaTimeType.deltaTime, AnimationType.Simple, curve,
         ()=>
         {
             canonPivot.localPosition = Vector2.zero;
         }, duration);

    }

    public override void OnFingerDown(LeanFinger finger)
    {
        aiming = true;
        lookAtPosition = finger.GetWorldPosition(10,null);
        ui.FingerDown();
    }

    public override void OnFingerSet(LeanFinger finger)
    {
        lookAtPosition = finger.GetWorldPosition(10,null);
    }

    public override void OnFingerUp(LeanFinger finger)
    {
        aiming = false;
        Fire();
        ui.FingerUp();
    }


    protected override void Update()
    {
        base.Update();
        if(aiming)
        {
            UpdateLookDirection();
            ui.SetTargetPoint(lookAtPosition);
        }
    }

    private float vel;
    private void UpdateLookDirection()
    {
        canonPivot.rotation = Quaternion.Euler(0, 0, Mathf.SmoothDampAngle(canonPivot.rotation.eulerAngles.z, GetAngle(),ref vel, smoothTime,aimingSpeed, Time.deltaTime));
    }

    private float GetAngle()
    {
        diference = lookAtPosition - (Vector2)transform.position;
        dir = diference.normalized;
        return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    }

    public override void OnActive()
    {
        base.OnActive();
        if(ui == null)
        {
            ui = Instantiate(uiPrefab, null).GetComponent<AlphaCannonUI>();
            ui.cannon = this;
        }
    }
}
