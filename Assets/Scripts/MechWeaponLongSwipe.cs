﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public abstract class MechWeaponLongSwipe : MechWeapon
{

    private bool onProcess = false;

    public override void OnActive()
    {
        if (IsWeaponActive) return;
        LeanTouch.OnFingerDown += ReadFingerDown;
        LeanTouch.OnFingerUp += ReadFingerUp;
        LeanTouch.OnFingerSet += ReadFingerSet;
    }

    public override void OnInactive()
    {
        if (!IsWeaponActive) return;
        LeanTouch.OnFingerDown -= ReadFingerDown;
        LeanTouch.OnFingerUp -= ReadFingerUp;
        LeanTouch.OnFingerSet -= ReadFingerSet;
    }

    public void ReadFingerDown(LeanFinger finger)
    {
        if (finger.IsOverGui) return;
        onProcess = true;
        OnFingerDown(finger);
    }

    public void ReadFingerSet(LeanFinger finger)
    {
        if (!onProcess) return;
        OnFingerSet(finger);
    }

    public void ReadFingerUp(LeanFinger finger)
    {
        if (!onProcess) return;
        OnFingerUp(finger);
    }

    public abstract void OnFingerDown(LeanFinger finger);

    public abstract void OnFingerSet(LeanFinger finger);

    public abstract void OnFingerUp(LeanFinger finger);
}
