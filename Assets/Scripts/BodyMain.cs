﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

/// <summary>
/// Central body component, if the health of this component reaches 0 the unit is dead. When a "Bodypart" receives damage, it spreads to the this one.
/// </summary>
public class BodyMain : SerializedMonoBehaviour, IInitialize, IUseHealth
{
    [SerializeField]
    private Health health = new Health();

    public Health Health
    {
        get
        {
            return health;
        }
    }

    [SerializeField, ReadOnly]
    private List<BodyPart> bodyParts;
 
    public void Initialize()
    {
        //FindPartsInHierarchy(transform);
        health.Initialize();
        foreach (BodyPart bp in bodyParts)
        {
            bp.Initialize();
        }
    }

    private void OnValidate()
    {
        if(Application.isEditor)
        {
            FindBodyPartsInHierarchy();
        }
    }

    [Button]
    private void FindBodyPartsInHierarchy()
    {
        bodyParts = new List<BodyPart>();
        FindPartsInHierarchy(transform);
    }


    private void FindPartsInHierarchy(Transform t)
    {
        Transform child;
        BodyPart bp;
        for (int i = 0; i < t.childCount; i++)
        {
            child = t.GetChild(i);
            Debug.Log("Child N°" + i + child);

            bp = child.GetComponent<BodyPart>();
            if (bp != null)
            {
                bp.main = this;
                bodyParts.Add(bp);
            }
            FindPartsInHierarchy(child);
        }
    }

}
