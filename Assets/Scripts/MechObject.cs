﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

[CreateAssetMenu(fileName = "Mech", menuName = "Units/Mech")]
public class MechObject : SerializedScriptableObject, IInitialize
{
    [ShowInInspector,ReadOnly]
    private MechUnit mUnit;


    [OnValueChanged("ValidatePrefab")]
    public GameObject mechPrefab;


    public void Initialize()
    {
        mUnit = Instantiate(mechPrefab, null).GetComponent<MechUnit>();
        mUnit.Initialize();
        mUnit.gameObject.SetActive(false);
    }


    void ValidatePrefab()
    {
        if(mechPrefab.GetComponent<MechUnit>() == null)
        {
            mechPrefab = null;
            Debug.LogError("Incompatible Prefab");
        }
    }

    public void DeployMech(Vector2 pos)
    {
        mUnit.Appear(pos);
        mUnit.gameObject.SetActive(true);
    }

}
