﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class Unit : SerializedMonoBehaviour, IInitialize, IUseHealth, IAppear {

    public Rigidbody2D rb;

    public bool initializeOnStart = false;


    [SerializeField,TabGroup("Movement")]
    private UnitMovement movement;

    [TabGroup("Health")]
    public BodyMain bodyMain;

    public Health Health
    {
        get
        {
            return bodyMain.Health;
        }
    }

    private void Start()
    {
        if(initializeOnStart)
        {
            Initialize();
        }
    }

    [Button]
    public void Initialize()
    {
        movement.Initialize(transform,rb);
        bodyMain.Initialize();
        Health.onLifeChangeEvent += OnLifeChangeEvent;
        Health.onDeadEvent += OnDead;
        Health.onDamageEvent += OnDamage;
        Health.onHealEvent += OnHeal;
    }

    public void FixedUpdate()
    {
        movement.FixedUpdate();
    }

    public abstract void OnDead();

    public abstract void OnLifeChangeEvent(int ammount);

    public abstract void OnDamage(int ammount, Vector2 sourcePosition);

    public abstract void OnHeal(int ammount);

    public abstract void Appear(Vector2 pos);

    public abstract void Disappear();
}
