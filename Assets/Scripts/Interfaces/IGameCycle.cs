﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameCycle
{

    void GC_PreStart();
    void GC_Start();
    void GC_PauseResume(bool value);
    void GC_End();
    void GC_PostEnd();

}
