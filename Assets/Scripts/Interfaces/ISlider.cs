﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISlider
{
    void SetSliderValue(float value);
}
