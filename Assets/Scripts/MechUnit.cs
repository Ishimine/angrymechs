﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechUnit : Unit
{
    public override void Appear(Vector2 pos)
    {
    }

    public override void Disappear()
    {
    }

    public override void OnDamage(int ammount, Vector2 sourcePosition)
    {
    }

    public override void OnDead()
    {
    }

    public override void OnHeal(int ammount)
    {
    }

    public override void OnLifeChangeEvent(int ammount)
    {
    }
}
