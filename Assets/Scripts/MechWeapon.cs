﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Lean.Touch;

public abstract class MechWeapon : SerializedMonoBehaviour
{
    [HideInInspector]
    public VoidEvent FireEvent;
    [HideInInspector]
    public VoidEvent ReloadStartedEvent;
    [HideInInspector]
    public VoidEvent ReloadDoneEvent;
    private bool reloading = false;

    [TabGroup("Firing")]
    public float fireRate = .2f;
    [TabGroup("Firing"),ReadOnly, ShowInInspector]
    protected float fireTimer;
    [TabGroup("Firing")]
    public float reloadTime = 3f;
    [TabGroup("Firing"), ReadOnly, ShowInInspector]
    protected float reloadTimer;
    [TabGroup("Firing")]
    public float mountingTime = 1.2f;

    [TabGroup("Firing")]
    public int maxAmmo = 7;
    [TabGroup("Firing")]
    public int currentAmmo = 7;


    public virtual bool CanFire
    {
        get
        {
            return (IsWeaponActive && fireTimer >= fireRate && currentAmmo > 0);
        }
    }

    public Collider2D col;

    [SerializeField]
    private bool isWeaponActive = false;
    public bool IsWeaponActive
    {
        get
        {
            return isWeaponActive;
        }
    }

    [SerializeField]
    private bool isReady = false;
    public bool IsReady
    {
        get
        {
            return isReady;
        }
    }

    [Button]
    public virtual void Fire()
    {
        currentAmmo--;
        fireTimer = 0;

    }
    

    [Button]
    public void SetWeaponActive()
    {
        SetWeaponActive(true);
    }

    [Button]
    public  void SetWeaponInactive()
    {
        SetWeaponActive(false);
    }

    public void SetWeaponActive(bool value)
    {
        if (value)
            OnActive();
        else
            OnInactive();

        isWeaponActive = true;
    }

    public abstract void OnActive();
    public abstract void OnInactive();


    protected virtual void Update()
    {
        if(!CanFire)
        {
            fireTimer += Time.deltaTime;

            if (!reloading && currentAmmo <= 0)
            {
                StartReload();
            }
            else if (reloading)
            {
                reloadTimer += Time.deltaTime;
                if (reloadTimer >= reloadTime)
                    ReloadDone();
            }
        }
        
    }

    [Button]
    protected void StartReload()
    {
        reloadTimer = 0;
        reloading = true;
        if (ReloadStartedEvent != null) ReloadStartedEvent.Invoke();
    }

    [Button]
    protected void ReloadDone()
    {
        reloading = false;
        currentAmmo = maxAmmo;
        reloadTimer = 0;
        if (ReloadDoneEvent != null) ReloadDoneEvent.Invoke();
    }
}
