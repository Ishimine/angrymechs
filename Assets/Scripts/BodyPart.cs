﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BodyPart : SerializedMonoBehaviour, IUseHealth, IInitialize
{
    public BodyMain main;

    [SerializeField]
    private Health health = new Health();
    public Health Health
    {
        get
        {
            return health;
        }
    }

    public void Initialize()
    {
        if (main == null) Debug.LogError("BodyMain is empty");


        health.Initialize();
        health.onDamageEvent += OnDamage;
    }

    void OnDamage(int ammount, Vector2 sourcePosition)
    {
        main.Health.Damage(ammount, sourcePosition);
    }


}
