﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class CameraController : SerializedMonoBehaviour
{
   public enum Cameras {Main, Second,Third}


    [SerializeField,ReadOnly]
    private Camera[] cameras;

    [SerializeField,OnValueChanged("UpdateCameras")]
    private Camera mainCamera;
    public Camera MainCamera
    {
        get
        {
            return mainCamera;
        }
    }
    [SerializeField,OnValueChanged("UpdateCameras")]
    private Camera secondCamera;
    public Camera SecondCamera
    {
        get
        {
            return secondCamera;
        }
    }
    [SerializeField,OnValueChanged("UpdateCameras")]
    private Camera thirdCamera;
    public Camera ThirdCamera
    {
        get
        {
            return thirdCamera;
        }
    }

    private void UpdateCameras()
    {
        cameras = new Camera[3];
        cameras[0] = mainCamera;
        cameras[1] = secondCamera;
        cameras[2] = thirdCamera;
    }

    public void SetTarget(Cameras camera, Transform target)
    {

    }


    private void SetPrimaryCameraTarget()
    {

    }

}
