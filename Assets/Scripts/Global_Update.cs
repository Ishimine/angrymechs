﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global_Update : MonoBehaviour
{
    public delegate void DeltaEvent(float deltaTime);
    public delegate void VoidEvent();

    public DeltaEvent gUpdate;
    public DeltaEvent gLateUpdate;
    public DeltaEvent gFixedUpdate;

    public static Global_Update instance;

    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
        
        gUpdate = null;
        gLateUpdate = null;
        gFixedUpdate = null;
    }

	void Update ()
    {
        if (gUpdate != null) gUpdate.Invoke(Time.deltaTime);
    }

    void LateUpdate()
    {
        if (gLateUpdate != null) gLateUpdate.Invoke(Time.deltaTime);
    }

    void FixedUpdate()
    {
        if (gFixedUpdate != null) gFixedUpdate.Invoke(Time.deltaTime);
    }
}
