﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

public class Health : IInitialize
{

    [HideInInspector]
    public VoidEventIntVector2 onDamageEvent;
    [HideInInspector]
    public VoidEventInt onHealEvent;
    [HideInInspector]
    public VoidEventInt onLifeChangeEvent;
    [HideInInspector]
    public VoidEvent  onDeadEvent;

    public void Initialize()
    {
        FullHeal();
        onDamageEvent = null;
        onHealEvent = null;
        onLifeChangeEvent = null;
        onDeadEvent = null;
    }

    public void Initialize(int maxHealth)
    {
        this.maxHealth = maxHealth;
        Initialize();
    }

    [SerializeField]
    private int currentHealth = 3;
    public int CurrentHealth
    {
        get
        {
            return currentHealth;
        }
    }

    [SerializeField]
    private int maxHealth = 3;
    public int MaxHealth
    {
        get
        {
            return maxHealth;
        }
    }

    /// <summary>
    /// Returns true if target is dead
    /// </summary>
    /// <param name="ammount"></param>
    /// <returns></returns>
    public bool Damage(int ammount, Vector2 sourcePosition)
    {
        currentHealth -= ammount;
        if (currentHealth < 0)
            currentHealth = 0;

        if (ammount != 0)
        {
            if (onDamageEvent != null) onDamageEvent(ammount,sourcePosition);
            if (onLifeChangeEvent != null) onLifeChangeEvent(-ammount);
        }
        bool isDead = !IsAlive();

        if (isDead)
        {
            Dead();
        }

        return isDead;
    }

    private void Dead()
    {
        if (onDeadEvent != null)
            onDeadEvent.Invoke();
    }

    [Button]
    public void FullHeal()
    {
        currentHealth = maxHealth;
    }

    [Button]
    private void GetOneDamage()
    {
        Damage(1,new Vector2());
    }

    [Button]
    private void GetOneHeal()
    {
        Heal(1);
    }

    /// <summary>
    /// Returns true if the target has MaxHealth
    /// </summary>
    /// <param name="ammount"></param>
    /// <returns></returns>
    public bool Heal(int ammount)
    {
        currentHealth += ammount;
        if (currentHealth > MaxHealth)
            currentHealth = MaxHealth;


        if (ammount != 0)
        {
            if (onHealEvent != null) onHealEvent(ammount);
            if (onLifeChangeEvent != null) onLifeChangeEvent(ammount);
        }

        return IsMaxHealth();
    }

    public bool IsAlive()
    {
        return currentHealth > 0;
    }

    public bool IsMaxHealth()
    {
        return currentHealth == maxHealth;
    }

   
}


public delegate void VoidEventInt(int value);
public delegate void VoidEventVector2(Vector2 value);
public delegate void VoidEventIntVector2(int ammount, Vector2 value);
public delegate void VoidEvent();
