﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ProyectilSimple : Proyectile
{
    public Collider2D col;
    public TrailRenderer trail;
    public GameObject explotionPrefab;

    public float speed;
    public override float Speed
    {
        get
        {
            return speed;
        }
    }

    public override Vector2 Direction
    {
        get
        {
            return transform.right;
        }
    }

    [SerializeField]
    private int damage;
    public override int Damage
    {
        get
        {
            return damage;
        }
    }

    public override void Activate()
    {
        col.isTrigger = true;
        base.Activate();
        trail.Clear();
        trail.gameObject.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IUseHealth h = collision.GetComponent<IUseHealth>();
        if(h != null)
        {
            h.Health.Damage(Damage, transform.position);
            Contact();
        }
    }

    public override void Contact()
    {
        base.Contact();
        Instantiate(explotionPrefab, null).transform.position = transform.position;

        trail.gameObject.transform.SetParent(null);

        Destroy(gameObject);
    }
}
