﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class WeaponCanvas: SerializedMonoBehaviour
{
    //public GameObject prefab;

    public abstract void SetCanvasActive();

    public abstract void SetCanvasInactive();

}
