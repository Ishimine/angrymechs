﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAppear
{
    void Appear(Vector2 pos);
    void Disappear();
}
