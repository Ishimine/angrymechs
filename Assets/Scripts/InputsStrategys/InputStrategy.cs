﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class InputStrategy<T> : SerializedScriptableObject
{
    public delegate void ActionEvent(T functionResponse);

    ActionEvent action;

    public virtual void Initialize(ActionEvent fireFunction)
    {
        action = fireFunction;
    }

    protected void Action(T value)
    {
        if (action != null)
            action.Invoke(value);
    }

}
