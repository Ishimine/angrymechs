﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
public abstract class MechWeaponTap : MechWeapon
{
    public override void OnActive()
    {
        if (IsWeaponActive) return;
        LeanTouch.OnFingerTap += OnFingerTap;
    }

    public override void OnInactive()
    {
        if (!IsWeaponActive) return;
        LeanTouch.OnFingerTap -= OnFingerTap;
    }

    private void OnFingerTap(LeanFinger finger)
    {
        OnTap(finger.GetWorldPosition(10,null), finger.ScreenPosition, finger.TapCount);
    }

    public abstract void OnTap(Vector2 worldPos, Vector2 screenPos, int count);

    
}
