﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class UnitMovement: IInitialize
{
    [SerializeField]
    protected Transform root;

    public float stopDistance = 1;


    [SerializeField]
    private bool canMove = false;
    public bool CanMove
    {
        get
        {
            return canMove;
        }
    }

    [HideInInspector]
    public VoidEvent onPositionReached;

    public bool useManualTarget;

    public Vector2 manualTargetPos;

    public Transform targetTransform;

    public Vector2 TargetPosition
    {
        get
        {
            if(useManualTarget)
            {
                return manualTargetPos;
            }
            else if(targetTransform != null)
            {
                return targetTransform.position;
            }
            else
            {
                return root.transform.position;
            }
        }
    }
   

    public void FixedUpdate()
    {
        if(canMove)
        {
            if(TargetPositionReached())
            {
                if (onPositionReached != null) onPositionReached.Invoke();
            }
            else
                MovementStrategy();
        }
    }

    public bool TargetPositionReached()
    {
        return Vector2.Distance(root.transform.position, TargetPosition) <= stopDistance;
    }

    public abstract void MovementStrategy();

    public void Activate()
    {
        canMove = true;
    }

    public void Deactivate()
    {
        canMove = false;
    }

    /// <summary>
    /// NO USAR!!!!!!!!!!!!!!!!
    /// Usar Initialize(Transform t)
    /// NO USAR!!!!!!!!!!!!!!!!
    /// </summary>
    public void Initialize()
    {
        onPositionReached = null;
        //Global_Update.instance.gFixedUpdate += FixedUpdate;
        Activate();
    }

    public virtual void Initialize(Transform t, Rigidbody2D rb)
    {
        root = t;
        Initialize();
    }


    public Vector2 GetTargetRawDirection()
    {
        return TargetPosition - (Vector2)root.position;
    }
}


