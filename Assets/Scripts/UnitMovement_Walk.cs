﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMovement_Walk : UnitMovement
{
    private Rigidbody2D rb;
    public float speed = 5;

    public override void MovementStrategy()
    {
        root.position += (Vector3)GetTargetRawDirection().normalized * speed * Time.fixedDeltaTime;
        //rb.MovePosition(root.position + (Vector3)GetTargetRawDirection().normalized * speed * Time.fixedDeltaTime);

        //rb.AddForce((Vector3)GetTargetRawDirection().normalized * speed,ForceMode2D.)
    }

    public override void Initialize(Transform t, Rigidbody2D rb)
    {
        this.rb = rb;
        base.Initialize(t, rb);
    }

}
