﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "CameraManager", menuName = "Managers/Camera")]
public class CameraManager : SerializedScriptableObject, IInitialize
{
    public GameObject cameraPrefab;

    public CameraController cController;


    [Button]
    public void Initialize()
    {
        GameObject go = GameObject.FindGameObjectWithTag("MainCamera");
        if(go == null)
            go = Instantiate(cameraPrefab, null);

        cController = go.GetComponent<CameraController>();
    }



}