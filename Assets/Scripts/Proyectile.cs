﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class Proyectile : SerializedMonoBehaviour
{

    private bool isActive;
    [HideInInspector]
    public VoidEvent onCollitionEvent;


    public abstract int Damage
    {
        get;
    }

    public abstract float Speed
    {
        get;
    }

    public abstract Vector2 Direction
    {
        get;
    }


    public void FixedUpdate()
    {
        if (!isActive) return;
        transform.position += (Vector3)Direction * Speed * Time.fixedDeltaTime;
    }


    [Button]
    public virtual void Activate()
    {
        gameObject.SetActive(true);
        isActive = true;
    }

    [Button]
    public virtual void Deactivate()
    {
        isActive = false;
    }

    [Button]
    public virtual void Contact()
    {
        if (onCollitionEvent != null) onCollitionEvent.Invoke();
    }
}
